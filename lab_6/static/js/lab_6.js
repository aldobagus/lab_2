//Chat-box
$(document).ready(function(){
    $(".arrow").click(function(){
        $(".chat-body").toggle();
    });
});


// Calculator
var print = document.getElementById("print");
var erase = false;

var go = function(x) {
  if (x === "ac") {
    /* implemetnasi clear all */
    print.value = null;
  } else if (x === "eval") {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if(x == "log"){
        print.value = Math.log10(print.value);
  } else if(x=="sin"){
        print.value = Math.sin(print.value);
  } else if(x=="tan"){
        print.value = Math.tan(print.value);
  }else if (erase === true){
      print.value = x;
      erase = false;
  } else {
      print.value += x;
  }
};

function evil(fn) {
  return new Function("return " + fn)();
}
// END


storage = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]
var backgroundColor;
var textColor;
var index;
//fungsi ganti tema
function changeTheme(bColor, tColor){
  $("body").css({"backgroundColor": bColor});
  $("h1").css({"color": tColor});
}
//Implementasi pergantian tema
if (localStorage.getItem("index") === null){
  index = 3;
}else{
  index = JSON.parse(localStorage.getItem("index"));
}
backgroundColor = storage[index]["bcgColor"];
textColor = storage[index]["fontColor"];
console.log(storage[index]["bcgColor"]);
changeTheme(backgroundColor, textColor);

$(document).ready(function(){
  $(".my-select").select2({
    "data" : storage
  });
  $(".my-select").val(index).change();
  $(".apply-button").on("click", function(){  // sesuaikan class button
      // [TODO] ambil value dari elemen select .my-select
      index = $(".my-select").val();
      // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
      backgroundColor = storage[index]["bcgColor"];
      textColor = storage[index]["fontColor"];
      // [TODO] ambil object theme yang dipilih
      // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
      changeTheme(backgroundColor, textColor);
      // [TODO] simpan object theme tadi ke local storage selectedTheme
      localStorage.setItem("index", JSON.stringify(index));
  })
});

var sender = true;

$('textarea').keypress(function(e){
    if(e.keyCode == 13 && !e.shiftKey) {
        var c = String.fromCharCode(e.which);
        var textValue = $('textarea').val();
        var fulltext = textValue + c;
        e.preventDefault();
        $('textarea').val('');

        if(sender){
            $('.msg-insert').append('<div class="msg-send">' + fulltext + '</div>');
            sender = false;
        }else{
            $('.msg-insert').append('<div class="msg-receive">' + fulltext + '</div>');
            sender = true;
        } 
    }
});